const app = require("express")();
const port = process.env.PORT || 5000;
const server = require("http").createServer(app);
const io = require("socket.io").listen(server).sockets;

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

let connectedUser = [];

// socket.io connect
io.on("connection", socket => {
  console.log("user connected");
  updateUserName();
  let userName = "";
  //login
  socket.on("login", name => {
    userName = name;
    connectedUser.push(userName);
    // console.log(connectedUser);
    updateUserName();
  });

  // Receive chat message
  socket.on("chat_message", msg => {
    console.log({
      name: userName,
      message: msg
    });
    let msgObj = {
      name: userName,
      message: msg
    };
    io.emit("user_msg", msgObj);
  });

  //  Disconnect
  socket.on("disconnect", () => {
    console.log("user disconnect");
    connectedUser.splice(connectedUser.indexOf(userName), 1);
    // console.log(connectedUser);
    updateUserName();
  });
});
const updateUserName = () => {
  io.emit("loadUser", connectedUser);
};
server.listen(port, () => {
  console.log("connected");
});
